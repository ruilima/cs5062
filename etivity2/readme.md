# Prepare the environment

```shell
#windows 
python -m venv venv;
venv\Scripts\activate;
python -m pip install -r requirements.txt;
python -m ipykernel install --user --name=projectenv --display-name="DataAnalytics E2";
pycharm .;
```

```bash 
# linux
python3 -m venv venv;
source venv/bin/activate;
python -m pip install -r requirements.txt;
python -m ipykernel install --user --name=projectenv --display-name="DataAnalytics E2";
pycharm .;
```
